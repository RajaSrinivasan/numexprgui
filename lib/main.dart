import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:group_button/group_button.dart';

import 'package:numexpr/numexpr.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Arithmetic Expression Evaluation'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  String newexpr ='?';
  int numexprval = 0;
  bool goodanswer = true ;

  var _gen = Generator() ;
  final myController = TextEditingController();

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  void _generateExpression() {
    setState(() {

      var e1 = _gen.Next() ;
      var e2 = _gen.Next() ;
      //var e3 = _gen.NextwBoth(e1,e2) ;
      newexpr = e2.getImage() ;
      numexprval = e2.Evaluate().toInt() ;
      myController.clear();
    }
    );
  }
  void _checkAnswer() {
    if (myController.text == 'Your answer') {
      return ;
    }
    var ans=int.parse(myController.text) ;
    if (ans == numexprval) {
      print ('Congrats. Good answer') ;
      goodanswer = true ;
    } else {
      goodanswer = false ;
      print( 'Back to school. try again');
    }

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Solve the problem :',
              style: Theme.of(context).textTheme.headline4,
            ),
            Text(
              '$newexpr' ,
              style: Theme.of(context).textTheme.headline4,
            ),

            //Icon(Icons.check , color : goodanswer ? Colors.Red , Colors.white) ,
            Icon( goodanswer ?  Icons.check : Icons.question_mark , color : goodanswer ? Colors.green : Colors.red , size : 50) ,
            TextFormField(
              style: Theme.of(context).textTheme.headline4,
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Your answer',
              ),
              controller: myController,
              keyboardType: TextInputType.numberWithOptions(signed: true,),
            ),

            GroupButton(
              isRadio: true,
              //onSelected: (index, isSelected) => print('$index button is selected'),
              onSelected: (val, i, selected) => {
                  //debugPrint('Button: $val index: $i $selected');
                  if (val == 'Next') {
                    _generateExpression()
                  },
                  if (val == 'Check') {
                    _checkAnswer()
                  },
                  setState(() => {} )
              } ,
              buttons: ["Check","Next"],
            ),
          ],
        ),
      ),


    );
  }
}
